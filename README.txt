This is the git repository for the course Econometrics of Program Evaluation.
These are all .rnw files containing latex and r code and to be compiled with knitr.
The files ending with '_slides' are the files generating the slides seen in class.
The other files are files for the chapters that have been written so far (corresponding mainly to Lecture0 for the moment (ferbuary 2016)).